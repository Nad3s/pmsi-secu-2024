# Pilotage de la maintenance des systemes d'information

Salut la team,

Si vous suivez ce cours, veuillez rejoindre ce projet avec votre compte.

* Ayez un compte gitlab
* Rejoignez ce projet (request access)
* Puis **une fois le projet fermé**:
  * Créez une branche préfixé `STU-` à votre **nom-prenom** : (exemple STU-simon-alan pour alan simon)
  * Dans cette branche un dossier à votre **nom-prenom dans le dossier students.**
  * Dans ce dossier un fichier me.txt de la forme :

```ascii
student : Nom Prenom
email : votre adresse mail
vous professionelement : 
 - tout ce qui est intéressant sur votre situation pro vos aspiration et vos attente
 - réalisation dons vous etes fier
 - votre next dans la liste des techno a voir 
evaluation du cour :
  toutes vos remarques sur le cours sont ici les bienvenues,
```

* Vous placerez dans ce dossier vos travaux
* Vous pousserez votre branche sur le dépot git du cours avant le dimanche qui suit chaque TP/TD à 16h. Tout travaux rendu après ne sera pas évalué.

## Rendus

Un fichier README.md sera présent à la racine de votre dossier personnel 
contenant un lien vers : 
* Les actions retenues dans votre contexte pour appliquer l’une des 7 PMD
* Les fonctions de « votre DSI »
* Les processus principaux de « votre DSI »
  * Les processsus de Gestion des Incidents, Changement, Probleme et Requetes adapté a votre DSI
Schéma, 
    * Présentation consise avec Raci, traces du traitement et KPI

exemple de schema de processus: ![process-exemple](./images/process-exemple.drawio.png)

## Règles 

### tenu de votre branche

**Le merge automatique doit être possible.** 

* Maintenez votre dossier personnel dans le dossier students
* Pas de modification à **l’extérieur** de votre dossier personnel
* Vous devez re-merger master avant de pousser votre branche

### redaction des documents

* En markdown, tout le monde devrais savoir faire du markdown
* Lien externe a votre branche refusé ( -8 point par lien externe ) tout est livré dans la branche
* schema sauvegardé dans le dépot avec un liens en markdown 

### travaux a plusieurs

Si vous avez travailler à plusieurs sur un document : 

* Assurez vous de ne pas travailler à plus de 2!
* Ajoutez les dans le premier chapitre de niveau 2 markdown du document tout les contributeurs en commençant par le rédacteur
* Livrer tous le même document exactement (md5sum)

```
# mon fichier

## contributeurs

nom-prenom
```

