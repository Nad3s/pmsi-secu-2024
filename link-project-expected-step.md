
## Maintenance

### Organiser le travail de maintenance du SI   

* monitoring / gestion des alertes -> court terme
* gestion des capacité -> long terme

### Elaborer la procédure de gestion des incidents

* cf processus de gestion des incidents

### Organisation des actions de formation.

* Matrice des compétences necessaire

### Elaborer un plan de gestion informatique

* on est plus sur la maintenance en terme d'évolution du SI 
  * le plan a long terme des evolutions :
    * avoir une Road Map fonctionnel et technique:
    * nouveau moteur de facturation 
    * gestion du renouvelement matériel / contrat de support -> asset managment

### Analyser l’impact financier :

* definir/maitenir/prévoir les coûts

## Coordonner les équipes : 

* gestion de la connaissance
* partages des documents , organisation commune des document
* Publication des processus des équipes
* organisation des reunions / ceremonie scrum

## Piloter l’organisation d’un système de collecte 

* monitoring


## Configurer un processus de communication

* communication service desk
  * publication du planning des maintenances
  * system de dissuasion du support      


## Piloter l’évolution du système d’information.

* Avoir une gestion des changements / CICD
  * planning des changement / release managment(cicd)
  * processus

# Stratégie du system d'information

## Elaborer la stratégie du SI à partir du diagnostic des besoins 

* definir clairement le besoin métier de l'exigence au souhait

## Déterminer la politique de sous-traitance et partenariale à mettre en place

* identifier clairement les fournisseurs et partie prenante
* negocier les contrats

## Etablir un plan d’activité et/ ou un plan d'urbanisation du SI

* orientation processus

